// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \ingroup OnePTwoCNIProblem
 * \brief OnePNCModel in combination with the NI model
 *
 * The simulation domain is a sqaure //TODO: add description
 */
#ifndef DUMUX_1P2CNI_PROBLEM_PROPERTIES_HH
#define DUMUX_1P2CNI_PROBLEM_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/box.hh>

#include <dumux/porousmediumflow/1pnc/model.hh>

#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include "problem.hh"
#include "spatialparams.hh"

namespace Dumux::Properties {
// Create new type tags
namespace TTag {
struct OnePTwoCNI { using InheritsFrom = std::tuple<OnePNCNI>; };
struct OnePTwoCNIBox { using InheritsFrom = std::tuple<OnePTwoCNI, BoxModel>; }; //box method for comparison with PNM
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePTwoCNI> { using type = Dune::YaspGrid<2>; };


// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePTwoCNI> { using type = OnePTwoCNIProblem<TypeTag>; };

template <class TypeTag> 
struct FluidSystem<TypeTag, TTag::OnePTwoCNI> 
{ 
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2OAir = FluidSystems::H2OAir<Scalar>; 
    static constexpr int phaseIdx = H2OAir::gasPhaseIdx; 
    using type = FluidSystems::OnePAdapter<H2OAir, phaseIdx>; //switch index of H2O and Air such that Air is phase and H2O the component
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePTwoCNI>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = OnePTwoCNISpatialParams<GridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::OnePTwoCNI> { static constexpr bool value = true; };

//TODO: Do I need this? What is it used for? -> set in CMakeLists.txt
#ifndef ENABLECACHING
#define ENABLECACHING false
#endif

// Switch on/off caching
template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::OnePTwoCNI> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::OnePTwoCNI> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::OnePTwoCNI> { static constexpr bool value = ENABLECACHING; };
} //End namespace Properties

#endif
