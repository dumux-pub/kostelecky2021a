// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \ingroup OnePTwoCNIProblem
 * \brief OnePTwoCModel in combination with the NI model
 *
 * The simulation domain is a square //TODO: write description of the problem
 */
#ifndef DUMUX_1P2CNI_PROBLEM_HH
#define DUMUX_1P2CNI_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
 * \ingroup OnePTwoCNIProblem
 * \brief OnePTwoCModel in combination with the NI model
 *
 * The simulation domain is a square //TODO: write description of the problem
 *
 * Initially, the domain is fully saturated air
 *
 * This problem uses the \ref OnePModel and \ref NIModel model.
 *
 */

template <class TypeTag>
class OnePTwoCNIProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView; 
    using Element = typename GridView::template Codim<0>::Entity;

    //TODO: following not used - do I need those?
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;

    // copy some indices for convenience
    enum
    {
        // indices of the primary variables
        pressureIdx = Indices::pressureIdx,
        temperatureIdx = Indices::temperatureIdx,

        // component indices
        H2OIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::H2OIdx),
        AirIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::AirIdx),
        phaseIdx = AirIdx, // = 0 (single phase system) //TODO: how to NOT hard set it...also with FluidSystem::phaseIdx(...)?

        // indices of the equations
        contiH2OEqIdx = Indices::conti0EqIdx + H2OIdx,
        contiAirEqIdx = Indices::conti0EqIdx + AirIdx,
        energyEqIdx = Indices::energyEqIdx
    };

    //! Property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>(); //TODO: not yet used, where do I need it?
    static const int dimWorld = GridView::dimensionworld; //TODO:not used
    using GlobalPosition = typename SubControlVolumeFace::GlobalPosition;

public:
    OnePTwoCNIProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        // get some parameters from the input file with default values
        pressureBottom_ = getParam<Scalar>("Problem.PressureBottom", 1e5);
        // moleFractionBottom_ = getParam<Scalar>("Problem.MoleFractionBottom"); //TODO:moleFraction OR Pressure, value for molefraction at bottom?
        temperatureBottom_ = getParam<Scalar>("Problem.TemperatureBottom", 293.15);

        pressureRefFF_ = getParam<Scalar>("Problem.PressureRefFF", 1e5);
        moleFracRefFFH2O_ = getParam<Scalar>("Problem.moleFracRefFFH2O", 0);
        temperatureRefFF_ = getParam<Scalar>("Problem.TemperatureRefFF", 293.15);

        boundaryLayerThickness_ = getParam<Scalar>("Problem.BoundaryLayerThickness", 1e-2);
        permeability_  = getParam<Scalar>("SpatialParams.Permeability", 1e-10);

        //get Indices from components and eq's --> output on console
        std::cout << "---------------- START INDICES OUTPUT ----------------" << std::endl;
        std::cout << "Air component index:" << AirIdx << std::endl;
        std::cout << "H2O component index:" << H2OIdx << std::endl;
        std::cout << "pressure index:" << pressureIdx << std::endl;
        std::cout << "Temperature index:" << temperatureIdx << std::endl;
        std::cout << "conti eq index for Air:" << contiAirEqIdx << std::endl;
        std::cout << "conti eq index for H2O:" << contiH2OEqIdx << std::endl;
        std::cout << "energy eq index:" << energyEqIdx << std::endl;
        std::cout << "---------------- END INDICES OUTPUT ----------------"<< std::endl;

        //initialize fluid system
        FluidSystem::init();

        // stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole fractions"<<std::endl;
        else
            std::cout<<"problem uses mass fractions"<<std::endl;
    }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;
        
        if (onLowerBoundary_(globalPos))
        { //mixed bc at bottom //TODO: decide if want to set dirichlet value for pressure or mole fraction
            bcTypes.setDirichlet(pressureIdx); // set pressure OR
            bcTypes.setNeumann(H2OIdx); //TODO: mole fraction of water in air x_{Air}^w index = H2O? OR bcTypes.setDirichlet(H2OIdx) + bcTypes.setNeumann(pressureIdx)
            bcTypes.setDirichlet(temperatureIdx);
        } else
        {
            bcTypes.setAllNeumann(); // everywhere else Neumann condition (no-flow at sides and bottom and non-zero neumann at top)
        }
        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars = initial_(globalPos);
        if(onLowerBoundary_(globalPos)){
            priVars[pressureIdx] = pressureBottom_;
            //priVars[] = moleFractionBottom_;
            priVars[temperatureIdx] = temperatureBottom_;
        }
        return priVars;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     */
    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables priVars(0.0);
        //TODO: add source for upper boundary depended on boundaryLayerThickness, p, p_Dirichlet, (x_Air^w, x_Dirichlet) T, T_Dirichlet
        Scalar flux = 0.0; //TODO: name it reasonable... later this is evaporation rate?
        if (onUpperBoundary_(globalPos))
        {
            // pressureDifference = pressure(scv) - p_ref --> elemVolVars //TODO: how to get elemVolVars(globalPos) bzw. scvf(globalPos)?
            // diffusionCoefficient, density -> fluidState
            // Scalar fluxAdvective = pressureDifference / length * permeability * moleFractionUpwind;
            // Scalar fluxDiffusive = moleFractionDifference / boundaryLayerThickness_ * diffusionCoefficient * density * area;
            // flux = fluxAdvective + fluxDiffusive;
        }
        priVars[contiAirEqIdx] = flux;
        return priVars;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub control volume face
     *
     * Negative values mean influx.
     * E.g. for the mass balance that would be the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables priVars(0.0);

        const auto& globalPos = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];

        Scalar airComponentFlux = 0.0; 
        Scalar h2oComponentFlux = 0.0;
        Scalar energyFlux       = 0.0;

        if (onUpperBoundary_(globalPos))
        {   
            Scalar moleFractionUpwindAir = 1.0;
            Scalar moleFractionUpwindH2O = 0.0;
            // get upwind value for mole fraction
            if(volVars.pressure(phaseIdx) > pressureRefFF_)
            {
                // take reference values from freeflow domain
                moleFractionUpwindAir = 1- moleFracRefFFH2O_;
                moleFractionUpwindH2O = moleFracRefFFH2O_;
            }else{
                // take values from boundary elements
                moleFractionUpwindAir = volVars.moleFraction(phaseIdx,AirIdx);
                moleFractionUpwindH2O = volVars.moleFraction(phaseIdx,H2OIdx);
            }

            Scalar diffusionCoeffH2O         = volVars.diffusionCoefficient(phaseIdx, AirIdx, H2OIdx); // diffusionCoefficient(phaseIIdx, compIIdx, compJIdx)
            Scalar molarMass                 = volVars.averageMolarMass(phaseIdx); //TODO: or use FluidSystem::molarMass(AirIdx) ?

            //calclulate advective fluxes (outflow > 0, inflow < 0)
            Scalar h2oFluxAdvective = (volVars.pressure(phaseIdx) - pressureRefFF_)
                                        /(boundaryLayerThickness_ + (globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()) 
                                        * permeability_ / volVars.viscosity(phaseIdx) 
                                        * volVars.molarDensity(phaseIdx) * moleFractionUpwindH2O; 
            Scalar airFluxAdvective = (volVars.pressure(phaseIdx) - pressureRefFF_) 
                                        /(boundaryLayerThickness_ + (globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()) 
                                        * permeability_/ volVars.viscosity(phaseIdx) 
                                        * volVars.molarDensity(phaseIdx) * moleFractionUpwindAir;
            
            //calclulate diffusive fluxes (outflow > 0, inflow < 0)
            Scalar h2oFluxDiffusive = (volVars.moleFraction(phaseIdx, H2OIdx) - moleFracRefFFH2O_) / boundaryLayerThickness_ 
                                        * diffusionCoeffH2O * volVars.molarDensity(phaseIdx);                     

            //calculate area specific, molar component fluxes [mol/(m^2*s)]
            //(Use mole fractions in the balance equations by default -> see pm/1pnc/model.hh)
            h2oComponentFlux = h2oFluxAdvective + h2oFluxDiffusive;
            airComponentFlux = airFluxAdvective; //TODO: no diffusive flux of comp air in phase air right?
            
            //calculate area specific energyflux [J/(m^2*s)] = [kg*m^2/s^2 * 1/(m^2*s)] = [kg/s^3] 
            energyFlux = FluidSystem::componentEnthalpy(volVars.fluidState(), phaseIdx, H2OIdx) * h2oComponentFlux * molarMass
                         + FluidSystem::componentEnthalpy(volVars.fluidState(), phaseIdx, AirIdx) * airComponentFlux * molarMass
                         + FluidSystem::thermalConductivity(volVars.fluidState(), phaseIdx) 
                            * (volVars.temperature() - temperatureRefFF_)
                            /(boundaryLayerThickness_ + (globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()) ; //TODO: units are now in [kg/s^3] do I have to use  [mol/s^3]?
        }

        //mole specific component fluxes (per default one balance for each component see pm/1pnc/model.hh)
        priVars[contiAirEqIdx] = airComponentFlux;
        priVars[contiH2OEqIdx] = h2oComponentFlux;
        //mole specific energy flux
        priVars[energyEqIdx]   = energyFlux;
        
        return priVars;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume.
     *
     * For this method, the \a priVars parameter stores the rate mass
     * of a component is generated or annihilated per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     *
     * The units must be according to either using mole or mass fractions (mole/(m^3*s) or kg/(m^3*s)).
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    { return NumEqVector(0.0); }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return initial_(globalPos); }

    /*!
    * \brief Set the simulation time.
    *
    * \param t The current time.
    */
    void setTime(Scalar t)
    { time_ = t; }

    // \}
private:

    // the internal method for the initial condition //TODO: values reasonable?
    PrimaryVariables initial_(const GlobalPosition& globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars[pressureIdx] = pressureBottom_;
        priVars[temperatureIdx] = temperatureBottom_;
        return priVars;
    }

    bool onLowerBoundary_(const GlobalPosition& globalPos) const
    {
        if(globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_)
            return true;
        else
            return false;
    }
    
    bool onUpperBoundary_(const GlobalPosition& globalPos) const
    {
        if(globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_)
            return true;
        else
            return false;
    }    

    static constexpr Scalar eps_ = 1e-6;
    Scalar time_;

    Scalar pressureBottom_;
    // Scalar moleFractionBottom_;
    Scalar temperatureBottom_;

    Scalar pressureRefFF_;
    Scalar moleFracRefFFH2O_;
    Scalar temperatureRefFF_;

    Scalar boundaryLayerThickness_;
    Scalar permeability_;
};
} // end namespace Dumux

#endif
