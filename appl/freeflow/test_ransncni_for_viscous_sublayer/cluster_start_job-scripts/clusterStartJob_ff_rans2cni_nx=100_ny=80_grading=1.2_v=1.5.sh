#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/mareike/DUMUX_AnnaMareike/kostelecky2021a
builddir=$moduledir/build-cmake
simdir=/temp/mareike/Output/nx=100_ny=80_grading=1.2_v=1.5            # Output/simulation directory

# given names
sourcedir=$moduledir/appl/freeflow/test_ransncni_for_viscous_sublayer   # path to source files (.hh-files, input file etc)
builddir=$builddir/appl/freeflow/test_ransncni_for_viscous_sublayer
executable=test_ff_rans2cni_komega            # ausführbare Datei
input=params_nonisothermal_nx=100_ny=80_grading=1.2_v=1.5.input              # Input-file name

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out #writes also in logfile
COMMAND="./$executable $input \
  | tee -a logfile.out" #writes every thing in logfile
echo $COMMAND > simulation.sh # writes COMMAND in simulation.sh file
chmod u+x simulation.sh # u+x => user/owner of files (u) gets execution permissions (+x)
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out

exit 0
