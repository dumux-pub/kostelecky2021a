#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# call by: python thickness_viscous_sublayer_overX.py -f ../csv-files/along_y\=0_thickness_vs/test_ff_rans2cni_v=1.5_komega.csv -v 1.5
#
import argparse
#from fileinput import filename
import sys
import os
import csv

import matplotlib.pyplot as plt
params = {'axes.labelsize': 16,
          'xtick.labelsize': 12,
          'ytick.labelsize': 12,
          'axes.titlesize': 18,
          'legend.fontsize': 14}
plt.rcParams.update(params)
import pandas as pd
import numpy as np


parser = argparse.ArgumentParser(
    prog="\033[1m\033[94m" + "python" + "\033[0m" + " " + sys.argv[0],
    description="Plot calculated and approximated thickeness of viscous sublayer",
)
parser.add_argument("-f", "--file", nargs="+", required=True, help="csv file to be processed")
parser.add_argument("-of", "--outFile", nargs=1, default="", help="Name of the written png file")
parser.add_argument("-v","--velocityInlet", nargs=1, required=True, type=float,help="csv file to be processed")
args = vars(parser.parse_args())

### Load data of csvs as dataframe.
#  pvpython ../../../../extractlinedata.py -f ./test_ff_rans2cni-00036.vtu -p1 0.0 0.0 0.0 -p2 6.0 0.0 0.0 -v 2
rel_path_to_input_file = os.path.relpath(args["file"][0])
data             = pd.read_csv(rel_path_to_input_file)

#set ouput filename
if args["outFile"]=="":
    filename        = os.path.splitext(os.path.basename(args["file"][0]))[0]
    output_filename = filename + "_thickness_vs_overX" + ".png"
else:
    filename = args["outFile"][0]
    output_filename = filename + "_thickness_vs_overX" + ".png"
rel_path_to_output_file = "../png-files/thickness_vs/" + output_filename

# extract specific data needed to calculate thickness of viscous sublayer
x = data["Points:0"] 
x = x.to_numpy()

dvx_dy = data["dv_x/dx_:1"]
dvx_dy = dvx_dy.to_numpy()

viscosity_nu = data["nu"]
viscosity_nu = viscosity_nu.to_numpy()

v_x_gas = data["velocity_gas (m/s):0"]

# set parameters for approximating thickness layer of viscous sublayer
y_vsPlus = 5
nu       = viscosity_nu[0] #kinematic viscosity at inlet
print("kinematic viscosity nu: ",nu)

vx_max   = args["velocityInlet"][0] # inlet velocity [m/s]
print(vx_max)

L_ch     = x   #TODO:length of channel right?
Re_x     = (vx_max * L_ch)/nu
print("Reynolds number Re_x at outlet = ", Re_x[-1])

c_f      = (2*np.log10(Re_x)-0.65)**(-2.3)

# calculate \delta_vs
delta_vs        = 5*np.sqrt(viscosity_nu)/np.sqrt(np.abs(dvx_dy))
delta_vs_approx = (y_vsPlus * nu)/(vx_max * np.sqrt(c_f/2))

######## plot \delta(x) for one specific velocity v
plt.figure(0)
plt.plot(x,delta_vs_approx, label=r"approximated $\delta_{vs}$")
plt.plot(x,delta_vs,label=r"calculated via $y^+ = 5$")
plt.xlabel(r'$x$')
plt.ylabel(r'$\delta_{vs}$')
plt.legend()
plt.title(r"Viscous sublayer thickness $\delta_{vs}(x)$ for $v_{inlet} =$" + str(vx_max) + "m/s")
plt.savefig(rel_path_to_output_file,bbox_inches='tight')


######### Find \delta(x=5) and write to csv.-file for different velocities ###########
idx = np.nanargmin(np.abs(x-5)) # get index of array where x = 5 (nearest)

header       = ["velocity_inlet","\delta_{vs}","viscosity_nu"]
data         = [vx_max, delta_vs[idx],nu]
csv_filename = "test_ff_rans2cni_komega.csv"
rel_path_to_csv_file = "../csv-files/y=0_x=5/" + csv_filename
file_exists  = os.path.isfile(rel_path_to_csv_file)

with open(rel_path_to_csv_file,"a") as f:
    writer = csv.writer(f)
    if not file_exists: # only writes header if file is new created
        writer.writerow(header)
    writer.writerow(data)