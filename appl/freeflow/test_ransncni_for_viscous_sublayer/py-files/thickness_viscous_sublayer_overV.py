#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# call by: python thickness_viscous_sublayer_overV.py -f ../csv-files/y\=0_x\=5/test_ff_rans2cni_komega.csv

import argparse
#from fileinput import filename
import sys
import os

import matplotlib.pyplot as plt
params = {'axes.labelsize': 16,
          'xtick.labelsize': 12,
          'ytick.labelsize': 12,
          'axes.titlesize': 18,
          'legend.fontsize': 14}
plt.rcParams.update(params)
import pandas as pd
import numpy as np


parser = argparse.ArgumentParser(
    prog="\033[1m\033[94m" + "python" + "\033[0m" + " " + sys.argv[0],
    description="Plot calculated and approximated thickeness of viscous sublayer",
)
parser.add_argument("-f", "--file", nargs="+", required=True, help="csv file to be processed")
parser.add_argument("-of", "--outFile", nargs=1, default="", help="Name of the written png file")
args = vars(parser.parse_args())

### Load data from csv-file created by thickness_viscous_sublayer_overX.py script
rel_path_to_input_file = os.path.relpath(args["file"][0])
data             = pd.read_csv(rel_path_to_input_file)

#set ouput filename
if args["outFile"]=="":
    filename        = os.path.splitext(os.path.basename(args["file"][0]))[0]
    output_filename = filename + "_thickness_vs_overV" + ".png"
else:
    filename = args["outFile"][0]
    output_filename = filename + "_thickness_vs_overV" + ".png"
rel_path_to_output_file = "../png-files/thickness_vs/" + output_filename
# extract specific data needed to calculate thickness of viscous sublayer
v_data = data["velocity_inlet"] 
v_data = v_data.to_numpy()

delta_data = data["\delta_{vs}"]
delta_data = delta_data.to_numpy()

nu_data = data["viscosity_nu"]

# set parameters for approximating thickness layer of viscous sublayer
y_vsPlus = 5
nu       = nu_data[0] #kinematic viscosity for all cases the same
print("kinematic viscosity nu: ",nu)

v = np.arange(0.05,4,0.01)

L_ch     = 5    #6 #TODO:length of channel right?
Re_x     = (v * L_ch)/nu
print("Reynolds number Re_x at outlet = ", Re_x[-1])

c_f      = (2*np.log10(Re_x)-0.65)**(-2.3)

# calculate \delta_vs
delta_vs_approx = (y_vsPlus * nu)/(v * np.sqrt(c_f/2))


#plot
plt.figure(0)
plt.plot(v,delta_vs_approx, label=r"approximated $\delta_{vs}$")
plt.plot(v_data,delta_data,"x",label=r"calculated via $y^+ = 5$")
plt.xlabel(r'velocity $v_x$')
plt.ylabel(r'$\delta_{vs}$')
plt.legend()
plt.title(r"Viscous sublayer thickness $\delta_{vs}(v)$ at $x = $" + str(L_ch) +"m")
plt.savefig(rel_path_to_output_file,bbox_inches='tight')