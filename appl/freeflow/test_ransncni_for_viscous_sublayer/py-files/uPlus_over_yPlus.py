#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 12:01:54 2020

@author: nedc
"""
import argparse
#from fileinput import filename
import sys
import os

import matplotlib.pyplot as plt
params = {'axes.labelsize': 19,
          'xtick.labelsize': 15,
          'ytick.labelsize': 15,
          'axes.titlesize': 22,
          'legend.fontsize':13,
          'legend.handlelength':2}
plt.rcParams.update(params)
import pandas as pd
import numpy as np


parser = argparse.ArgumentParser(
    prog="\033[1m\033[94m" + "python" + "\033[0m" + " " + sys.argv[0],
    description="Plot calculated and approximated thickeness of viscous sublayer",
)
parser.add_argument("-f", "--file", nargs="+", required=True, help="csv file to be processed")
parser.add_argument("-of", "--outFile", nargs=1, default="", help="Name of the written png file")
args = vars(parser.parse_args())

### Load data of csvs as dataframe.
#  pvpython ../../../../extractlinedata.py -f ./test_ff_rans2cni-00036.vtu -p1 5.0 0.0 0.0 -p2 5.0 0.5 0.0

### Load in all of the csvs as dataframes.
rel_path_to_input_file = os.path.relpath(args["file"][0])
komega_data            = pd.read_csv(rel_path_to_input_file)
# kepsilon = pd.read_csv('./csvs/dumux_kepsilon.csv')
# lowrekepsilon = pd.read_csv('./csvs/dumux_lowrekepsilon.csv')
# oneeq = pd.read_csv('./csvs/dumux_oneeq.csv')
# zeroeq = pd.read_csv('./csvs/dumux_zeroeq.csv')

### Load in all of the experimental result csvs as dataframes.
# exp_lauferpipe = pd.read_csv('./csvs/exp_lauferpipe.csv', comment='#')


#set ouput filename
if args["outFile"]=="":
    filename        = os.path.splitext(os.path.basename(args["file"][0]))[0]
    output_filename = filename + "_lawofthewall" + ".png"
else:
    filename = args["outFile"][0]
    output_filename = filename + "_lawofthewall" + ".png"
rel_path_to_output_file = "../png-files/law_of_wall/" + output_filename

# ##############################################################
# # Plot the velocity profile upstream of the step (x/H =-4) ###
# ##############################################################
plt.figure(1, figsize=(8, 6), dpi=100)

# experimental data
# exp_uplus = exp_lauferpipe['U+']
# exp_yplus = exp_lauferpipe['Y+']

# dumux data
komega_uplus = komega_data["u^+"]
komega_uplus = komega_uplus.to_numpy()
print(komega_uplus)
# kepsilon_uplus = kepsilon["u^+"]
# lowrekepsilon_uplus = lowrekepsilon["u^+"]
# oneeq_uplus = oneeq["u^+"]
# zeroeq_uplus = zeroeq["u^+"]

komega_yplus = komega_data["y^+"]
komega_yplus = komega_yplus.to_numpy()
print(komega_yplus)
# kepsilon_yplus = kepsilon["y^+"]
# lowrekepsilon_yplus = lowrekepsilon["y^+"]
# oneeq_yplus = oneeq["y^+"]
# zeroeq_yplus = zeroeq["y^+"]

# theory lines
visc_yplus = np.linspace(1,1200,1200)
visc_uplus = visc_yplus
turb_yplus = np.linspace(1,1200,1200)
turb_uplus = 1/0.41 * np.log(turb_yplus) + 5

# Plot lines
# plt.plot(exp_yplus,exp_uplus, '--r+', label="experiment")
plt.plot(komega_yplus,komega_uplus, 'x', label="K-omega")
# plt.plot(kepsilon_yplus,kepsilon_uplus, label="K-epsilon")
# plt.plot(lowrekepsilon_yplus,lowrekepsilon_uplus, label="Low Re K-epsilon")
# plt.plot(oneeq_yplus,oneeq_uplus, label="One-eq")
# plt.plot(zeroeq_yplus,zeroeq_uplus, label="Zero-eq")
plt.plot(visc_yplus,visc_yplus, '-*k', label="Linear Sublayer")
plt.plot(turb_yplus,turb_uplus, '--k', label= "van Driest Log-law")

vert_y = np.linspace(0,25,25)
visc_x = np.full(len(vert_y),5)
turb_x = np.full(len(vert_y),30)
plt.plot(visc_x,vert_y)
plt.plot(turb_x,vert_y)

# Format plot
plt.xscale("log")
plt.xlabel('$Y^+$')
plt.ylabel('$U^+$')
plt.text(1.1,24,'Viscous Sublayer')
plt.text(6.1,24,'Mixed Sublayer')
plt.text(100,24,'Turbulent layer')
plt.title('Law of the Wall')
plt.xlim(1,1200)
plt.ylim(0,25)
plt.legend()
plt.savefig(rel_path_to_output_file, bbox_inches='tight')
