// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the  one-phase two-component pore network model.
 */
#ifndef DUMUX_PNM1P2C_PROBLEM_HH
#define DUMUX_PNM1P2C_PROBLEM_HH

// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetwork/1pnc/model.hh>
// #include <dumux/discretization/porenetwork/gridgeometry.hh> //for getting poreGeometry

#include <dumux/common/boundarytypes.hh>
//for output at console
#include<iostream>

namespace Dumux
{
template <class TypeTag>
class PNMOnePTwoCNIProblem;

template <class TypeTag>
class PNMOnePTwoCNIProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Shape = Dumux::PoreNetwork::Pore::Shape;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using Labels = GetPropType<TypeTag, Properties::Labels>;
    enum {
        // component indices //TODO: use these also for setting mole fractions, right?
        H2OIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::H2OIdx), // Index 1
        AirIdx = FluidSystem::compIdx(FluidSystem::MultiPhaseFluidSystem::AirIdx), // Index 0 (main component)
        phaseIdx = AirIdx, // = 0 (single phase system) //TODO: how to NOT hard set it...also with FluidSystem::phaseIdx(...)?

        // primary variable indices
        temperatureIdx = Indices::temperatureIdx, //Index 2
        pressureIdx = Indices::pressureIdx, // Index 0

        // component eq indices
        contiAirEqIdx = Indices::conti0EqIdx + AirIdx, //Index 0
        contiH2OEqIdx = Indices::conti0EqIdx + H2OIdx, //Index 1
        energyEqIdx = Indices::energyEqIdx, // Index 2
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<GridView::dimension>::Entity;

public:
    template<class SpatialParams>
    PNMOnePTwoCNIProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<SpatialParams> spatialParams)
    : ParentType(gridGeometry, spatialParams)
    {
        // get some parameters from the input file
        pressureBottom_ = getParam<Scalar>("Problem.PressureBottom", 1e5);
        // moleFractionBottom_ = getParam<Scalar>("Problem.MoleFractionBottom"); //TODO:moleFraction OR Pressure
        temperatureBottom_ = getParam<Scalar>("Problem.TemperatureBottom", 293.15);

        pressureRefFF_ = getParam<Scalar>("Problem.PressureRefFF", 1e5);
        moleFracRefFFH2O_ = getParam<Scalar>("Problem.moleFracRefFFH2O", 0);
        temperatureRefFF_ = getParam<Scalar>("Problem.TemperatureRefFF", 293.15);

        boundaryLayerThickness_ = getParam<Scalar>("Problem.BoundaryLayerThickness", 1e-2);
        permeability_  = getParam<Scalar>("SpatialParams.Permeability", 1e-10);

        //get Indices from components and eq's --> output on console
        std::cout << "---------------- START INDICES OUTPUT ----------------" << std::endl;
        std::cout << "Air component index:" << AirIdx << std::endl;
        std::cout << "H2O component index:" << H2OIdx << std::endl;
        std::cout << "pressure index:" << pressureIdx << std::endl;
        std::cout << "Temperature index:" << temperatureIdx << std::endl;
        std::cout << "conti eq index for Air:" << contiAirEqIdx << std::endl;
        std::cout << "conti eq index for H2O:" << contiH2OEqIdx << std::endl;
        std::cout << "energy eq index:" << energyEqIdx << std::endl;
        std::cout << "---------------- END INDICES OUTPUT ----------------"<< std::endl;

        FluidSystem::init();
    }

     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolume& scv) const
    {
        BoundaryTypes bcTypes;
        
        if (onLowerBoundary_(scv))
        { //mixed bc at bottom //TODO: decide if want to set dirichlet value for pressure or mole fraction
            bcTypes.setDirichlet(pressureIdx); // set pressure OR
            bcTypes.setNeumann(H2OIdx); //TODO: mole fraction of water in air x_{Air}^w index = H2O? OR bcTypes.setDirichlet(H2OIdx) + bcTypes.setNeumann(pressureIdx)
            bcTypes.setDirichlet(temperatureIdx);
        } else
        {
            bcTypes.setAllNeumann(); // everywhere else Neumann no-flow condition (other neumann cond at top set by source())
        }
        return bcTypes;
    }

        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element& element,
                               const SubControlVolume& scv) const
    {
        PrimaryVariables priVars(0.0);
        if(onLowerBoundary_(scv)){
            priVars[pressureIdx] = pressureBottom_; //TODO: include pressureBottom in param-file and read value from param-file
            //priVars[] = moleFractionBottom_; //TODO: include moleFractionBottom in param-file and read value from param-file
            priVars[temperatureIdx] = temperatureBottom_; //TODO: include temperatureBottom in param-file and read value from param-file
        }
        return priVars;
    }

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub control volume
     *
     * For this method, the return parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    template<class ElementVolumeVariables>
    PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {
        PrimaryVariables priVars(0.0);

        // const auto& globalPos = scv.dofPosition(); //globalPosition at boundary is already dof Position --> don't have to be considered for gradients (\nabla p = \delta p/L)
        const auto& volVars = elemVolVars[scv]; //TODO: right?

        Scalar airComponentFlux = 0.0; 
        Scalar h2oComponentFlux = 0.0;
        Scalar energyFlux       = 0.0;

        if (onUpperBoundary_(scv))
        {   
            Scalar moleFractionUpwindAir = 1.0;
            Scalar moleFractionUpwindH2O = 0.0;
            // get upwind value for mole fraction
            if(volVars.pressure(phaseIdx) > pressureRefFF_)
            {
                // take reference values from freeflow domain
                moleFractionUpwindAir = 1- moleFracRefFFH2O_;
                moleFractionUpwindH2O = moleFracRefFFH2O_;
            }else{
                // take values from boundary elements
                moleFractionUpwindAir = volVars.moleFraction(phaseIdx,AirIdx);
                moleFractionUpwindH2O = volVars.moleFraction(phaseIdx,H2OIdx);
            }

            Scalar diffusionCoeffH2O         = volVars.diffusionCoefficient(phaseIdx, AirIdx, H2OIdx); // diffusionCoefficient(phaseIIdx, compIIdx, compJIdx)
            Scalar molarMass                 = volVars.averageMolarMass(phaseIdx); //TODO: or use FluidSystem::molarMass(AirIdx) ?

            //calclulate advective fluxes (outflow > 0, inflow < 0)
            Scalar h2oFluxAdvective = (volVars.pressure(phaseIdx) - pressureRefFF_) 
                                        /(boundaryLayerThickness_) // dof already at boundary, right? -> only \delta_vs needed
                                        * permeability_ / volVars.viscosity(phaseIdx) 
                                        * volVars.molarDensity(phaseIdx) * moleFractionUpwindH2O; 
            Scalar airFluxAdvective = (volVars.pressure(phaseIdx) - pressureRefFF_) 
                                        /(boundaryLayerThickness_)
                                        * permeability_/ volVars.viscosity(phaseIdx) 
                                        * volVars.molarDensity(phaseIdx) * moleFractionUpwindAir;
            
            //calclulate diffusive fluxes (outflow > 0, inflow < 0)
            Scalar h2oFluxDiffusive = (volVars.moleFraction(phaseIdx, H2OIdx) - moleFracRefFFH2O_) / boundaryLayerThickness_ 
                                        * diffusionCoeffH2O * volVars.molarDensity(phaseIdx);                     

            //calculate area specific, molar component fluxes [mol/(m^3*s)] (volume specific)
            //(Use mole fractions in the balance equations by default -> see pm/1pnc/model.hh)
            Scalar inscribedRadius = volVars.poreInscribedRadius();
            Shape shape = this->gridGeometry().poreGeometry(scv.dofIndex()); // direct through Shape shape = Shape::cube;
            Scalar poreCrossSectionalArea = poreCrossSectionalArea_(shape, inscribedRadius);

            h2oComponentFlux = -1*(h2oFluxAdvective + h2oFluxDiffusive) * poreCrossSectionalArea / scv.volume(); //inflow flux > 0, outflow flux < 0
            airComponentFlux = -1*(airFluxAdvective) * poreCrossSectionalArea / scv.volume(); //TODO: no diffusive flux of comp air in phase air right?
            
            //calculate area specific energyflux [J/(m^3*s)] = [kg*m^2/s^2 * 1/(m^3*s)] = [kg/(m*s^3)] 
            energyFlux = FluidSystem::componentEnthalpy(volVars.fluidState(), phaseIdx, H2OIdx) * h2oComponentFlux * molarMass
                         + FluidSystem::componentEnthalpy(volVars.fluidState(), phaseIdx, AirIdx) * airComponentFlux * molarMass
                         - FluidSystem::thermalConductivity(volVars.fluidState(), phaseIdx) 
                            * (volVars.temperature() - temperatureRefFF_) 
                            /(boundaryLayerThickness_ ) * poreCrossSectionalArea / scv.volume();
        }

        //mole specific component fluxes (per default one balance for each component see pm/1pnc/model.hh)
        priVars[contiAirEqIdx] = airComponentFlux;
        priVars[contiH2OEqIdx] = h2oComponentFlux;
        //mole specific energy flux
        priVars[energyEqIdx]   = energyFlux;
        
        return priVars;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Vertex& vertex) const
    {
        PrimaryVariables priVars(0.0);
        priVars[pressureIdx] = pressureBottom_;
        priVars[temperatureIdx] = temperatureBottom_;
        return priVars;
    }

private:
    bool onLowerBoundary_(const SubControlVolume& scv) const
    {
        if(scv.dofPosition()[1] < this->gridGeometry().bBoxMin()[1] + eps_)
            return true;
        else
            return false;
    }
    
    bool onUpperBoundary_(const SubControlVolume& scv) const
    {
        if(scv.dofPosition()[1] > this->gridGeometry().bBoxMax()[1] - eps_)
            return true;
        else
            return false;
    }

    inline Scalar poreCrossSectionalArea_(Shape shape, Scalar inscribedRadius) const 
    {
        switch(shape)
        {
            case Shape::cube: return 4.0*inscribedRadius*inscribedRadius; break; //crossSectionalArea is square
            case Shape::sphere: return M_PI*inscribedRadius*inscribedRadius; break; //crossSectionalArea is circle
            default : DUNE_THROW(Dune::InvalidStateException, "Unsupported geometry - only cube and sphere are supported  as pore shape so far");
        }
    }

    static constexpr Scalar eps_ = 1e-6; //TODO: reasonable?
    Scalar pressureBottom_;
    // Scalar moleFractionBottom_;
    Scalar temperatureBottom_;

    Scalar pressureRefFF_;
    Scalar moleFracRefFFH2O_;
    Scalar temperatureRefFF_;

    Scalar boundaryLayerThickness_;
    Scalar permeability_;
};
} //end namespace Dumux

#endif
