// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The properties for the  one-phase two-component pore network model.
 */
#ifndef DUMUX_PNM1P2C_PROPERTIES_HH
#define DUMUX_PNM1P2C_PROPERTIES_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/porenetwork/1pnc/model.hh>

#include <dumux/common/properties.hh>

// #include <dumux/material/fluidsystems/h2on2.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/material/fluidsystems/1padapter.hh>

#include "problem.hh"

//////////
// Specify the properties
//////////
namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct PNMOnePTwoCNIProblem { using InheritsFrom = std::tuple<PNMOnePNCNI>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::PNMOnePTwoCNIProblem> { using type = Dumux::PNMOnePTwoCNIProblem<TypeTag>; };

// Select the fluid system: Air as phase and component, Water as component
template <class TypeTag> 
struct FluidSystem<TypeTag, TTag::PNMOnePTwoCNIProblem> 
{ 
    using H2OAir = FluidSystems::H2OAir<GetPropType<TypeTag,Properties::Scalar>>; 
    static constexpr int phaseIdx = H2OAir::gasPhaseIdx; 
    using type = FluidSystems::OnePAdapter<H2OAir, phaseIdx>; //switch index of H2O and Air such that Air is phase and H2O the component
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::PNMOnePTwoCNIProblem> { using type = Dune::FoamGrid<1, 2>; }; //change dimension here

//! The advection type //TODO: is there anything that need to be changed here?
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::PNMOnePTwoCNIProblem>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TransmissibilityLaw = Dumux::PoreNetwork::TransmissibilityAzzamDullien<Scalar>;
public:
    using type = Dumux::PoreNetwork::CreepingFlow<Scalar, TransmissibilityLaw>;
};

} //end namespace Dumux::Properties

#endif
