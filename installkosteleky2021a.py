#!/usr/bin/env python3
#
# This installs the module kostelecky2021a and its dependencies.
# The exact revisions used should be listed in the table below. (TODO:)
#

import os
import sys
import subprocess

top = "DUMUX_AnnaMareike"
os.makedirs(top, exist_ok=True)

def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )

def installModule(subFolder, url, branch):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
    else:
        print(f'Skip cloning {url} since target folder "{targetFolder}" already exists.')


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dumux")
installModule("dumux", "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git", "master")

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git", "releases/2.8")

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "releases/2.8")

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "releases/2.8")

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "releases/2.8")

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "releases/2.8")

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "releases/2.8")

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "releases/2.8")

print("Installing kostelecky2021a")
installModule("kostelecky2021a", "https://git.iws.uni-stuttgart.de/dumux-pub/kostelecky2021a.git", "master")

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=./dumux/cmake.opts', 'all'],
    '.'
)
